using BotCore.DataModel;
using BotCore.Web.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BotCore.Web.Controllers.API;

[Controller]
[Route("/")]
public class Redirects : Controller
{
    private readonly Db _db;

    public Redirects(Db db)
    {
        _db = db;
    }

    [HttpGet("avatar.webp")]
    public object GetBot()
    {
        var bot = Resolvers.GetBotByHost(_db, Request.Host.Host);
        if (bot.AvatarUrl == null || bot.AvatarUrl.Length == 0)
            return Redirect("https://cdn.discordapp.com/embed/avatars/0.png");
        return Redirect(bot.AvatarUrl);
    }
}