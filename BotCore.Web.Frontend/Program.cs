using BotCore.Web.Frontend;
using Sentry;

var webCfg = Config.Read();
webCfg.Save();

var envname = webCfg.SentryEnvironment;
if (envname.Length < 1)
{
    Console.WriteLine("Environment name not set! Using hostname, to change this, set in Config.json!");
    envname = Environment.MachineName;
}

var builder = WebApplication.CreateBuilder(args);
builder.WebHost.UseSentry(o =>
{
    o.Dsn = "https://7f007f00a7d64ae697b061cf1f25739e@sentry.thearcanebrony.net/11";
    o.TracesSampleRate = 1.0;
    o.AttachStacktrace = true;
    o.MaxQueueItems = int.MaxValue;
    o.StackTraceMode = StackTraceMode.Original;
    o.Environment = envname;
});
// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddSentry();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseSentryTracing();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");
});

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();