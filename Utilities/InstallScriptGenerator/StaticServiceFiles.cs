using ArcaneLibs;

namespace InstallScriptGenerator;

public class StaticServiceFiles
{
    public static void MakeStaticServices(string[] args)
    {
        LegacyWebService(args);
        DataUpdaterService(args);
    }

    public static void LegacyWebService(string[] args)
    {
        Console.Error.WriteLine($"Writing service file: botcore.web...");
        var content = $@"[Unit]
Description=BotCore Web (legacy)

[Service]
User=root
WorkingDirectory={Environment.CurrentDirectory}/BotCore.Web.Legacy
ExecStart=/bin/dotnet run --nobuild
Restart=always
Type=notify
NotifyAccess=all

[Install]
WantedBy=multi-user.target";
        File.WriteAllText($"/etc/systemd/system/botcore.web.service", content);

        if (args.Contains("--enable")) Util.RunCommandSync("systemctl", "enable botcore.web", true);
        if (args.Contains("--start")) Util.RunCommandSync("systemctl", "start botcore.web", true);
    }

    public static void DataUpdaterService(string[] args)
    {
        Console.Error.WriteLine($"Writing service file: botcore.dataupdater...");
        var content = $@"[Unit]
Description=BotCore Data Updater

[Service]
User=root
WorkingDirectory={Environment.CurrentDirectory}/BotCore.DataUpdater
ExecStart=/bin/dotnet run --nobuild
Restart=always
RestartSec=600
Type=notify
NotifyAccess=all

[Install]
WantedBy=multi-user.target";
        File.WriteAllText($"/etc/systemd/system/botcore.dataupdater.service", content);

        if (args.Contains("--enable")) Util.RunCommandSync("systemctl", "enable botcore.dataupdater", true);
        if (args.Contains("--start")) Util.RunCommandSync("systemctl", "start botcore.dataupdater", true);
    }
}