using BotCore.DataModel;
using Microsoft.EntityFrameworkCore;

namespace BotCore.Web.Frontend;

public class Logic
{
    public static Db db = Db.GetPostgres();

    public static Bot GetBotByDomain(string dom, bool includeServers = false, bool includeUsers = false,
        bool includeQuotes = false)
    {
        var p = dom.Split('.');
        if (p.Length <= 1) p = new[] {"playground"};
        var dbb = db.Bots.AsQueryable();

        if (includeServers || includeUsers || includeQuotes)
        {
            dbb = dbb.Include(x => x.Servers);
            if (includeUsers || includeQuotes)
            {
                dbb = dbb.Include(x => x.Servers).ThenInclude(x => x.DServerUsers).ThenInclude(x=>x.GlobalUser);
                if (includeQuotes)
                    dbb = dbb.Include(x => x.Servers).ThenInclude(x => x.DServerUsers).ThenInclude(x => x.GlobalUser) 
                        .Include(x=>x.Servers).ThenInclude(x=>x.DServerUsers).ThenInclude(x=>x.Quotes);
            }
        }

        var bot = dbb.FirstOrDefault(x =>
            x.Name == p[0].ToLower() || x.FriendlyName.Replace(" ", "").Replace("/", "").ToLower() == p[0].ToLower());
        if (bot == null) throw new Exception($"Couldn't resolve bot from hostname: {dom}");
        return bot;
    }
}