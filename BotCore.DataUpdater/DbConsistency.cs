using BotCore.DataModel;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace BotCore.DataUpdater;

public class DbConsistency
{
    public static void Execute(Db db)
    {
        //ensure friendly name is set and name is valid:
        foreach (var dbBot in db.Bots)
            if (dbBot.FriendlyName == null || dbBot.FriendlyName.Length == 0)
            {
                dbBot.FriendlyName = dbBot.Name;
                dbBot.Name = dbBot.FriendlyName.Replace(" ", "").Replace("/", "").ToLower();
                Console.WriteLine($"{dbBot.FriendlyName} -> {dbBot.Name}");
            }

        //fix invalid xp states
        if (db.DServerUsers.AsEnumerable().Any(x => x.Xp >= x.XpGoal || x.Xp < 0))
            Console.WriteLine("Found invalid xp states!");
        foreach (var u in db.DServerUsers.AsEnumerable().Where(x => x.Xp >= x.XpGoal || x.Xp < 0))
            if (u.Xp >= u.XpGoal)
            {
                Console.WriteLine($"Levelling up user {u.UserId} ({u.Xp}/{u.XpGoal})");
                while (u.Xp >= u.XpGoal)
                {
                    u.Xp -= u.XpGoal;
                    u.XpLevel++;
                }
            }
            else if (u.Xp < 0)
            {
                Console.WriteLine($"Set xp to 0 for {u.UserId} ({u.Xp})");
                u.Xp = 0;
            }

        db.SaveChanges();
        //fix sequences in db
        var conn = (NpgsqlConnection) db.Database.GetDbConnection();
        conn.Open();
        if (db.GlobalUsers.Any())
            new NpgsqlCommand(
                $"alter sequence \"global_user_GlobalUserId_seq\" restart with {db.GlobalUsers.Max(x => x.GlobalUserId) + 1}",
                conn).ExecuteNonQuery();
        if (db.Bots.Any())
            new NpgsqlCommand($"alter sequence \"bot_BotId_seq\" restart with {db.Bots.Max(x => x.BotId) + 1}", conn)
                .ExecuteNonQuery();
        if (db.Servers.Any())
            new NpgsqlCommand(
                $"alter sequence \"server_ServerId_seq\" restart with {db.Servers.Max(x => x.ServerId) + 1}",
                conn).ExecuteNonQuery();
        if (db.DServerUsers.Any())
            new NpgsqlCommand(
                    $"alter sequence \"d_server_user_UserId_seq\" restart with {db.DServerUsers.Max(x => x.UserId) + 1}",
                    conn)
                .ExecuteNonQuery();
        if (db.Quotes.Any())
            new NpgsqlCommand($"alter sequence \"quote_QuoteId_seq\" restart with {db.Quotes.Max(x => x.QuoteId) + 1}",
                    conn)
                .ExecuteNonQuery();
        if (db.Warnings.Any())
            new NpgsqlCommand(
                $"alter sequence \"warning_WarningId_seq\" restart with {db.Warnings.Max(x => x.WarningId) + 1}",
                conn).ExecuteNonQuery();
        conn.Close();

        //delete all data for global users which dont exist anymore
        foreach (var u in db.DServerUsers.Include(x => x.GlobalUser).AsEnumerable().Where(x =>
                     x.GlobalUser == null ||
                     !Static.RoDB.GlobalUsers.Any(y => y.GlobalUserId == x.GlobalUser.GlobalUserId)))
            Console.WriteLine("Found invalid user ref, this shouldn't happen: " + u.UserId);
    }
}