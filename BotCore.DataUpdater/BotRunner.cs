using BotCore.DataModel;
using DSharpPlus;

namespace BotCore.DataUpdater;

public class BotRunner
{
    public static async Task<DiscordClient> RunBot(Db db, Bot bot)
    {
        var c = new DiscordClient(new DiscordConfiguration
        {
            Token = bot.Token,
            TokenType = TokenType.Bot,
            Intents = DiscordIntents.All,
            GatewayCompressionLevel = GatewayCompressionLevel.None
        });
        // cl.Add(c);
        var AvailableGuilds = 0;
        c.GuildDownloadCompleted += async (sender, _) =>
        {
            Console.WriteLine(
                $"[{bot.Name}] Fully logged in as {sender.CurrentUser.Username}#{sender.CurrentUser.Discriminator} {sender.Guilds.Count}");
            //update bot info
            bot.ServerCount = AvailableGuilds;
            bot.AvatarUrl = c.CurrentUser.GetAvatarUrl(ImageFormat.WebP);
            bot.CoreVer = "v4.0.0a0";
            //update server info
            Console.WriteLine($"[{bot.Name}] Checking {bot.Servers.Count(x => x.BotId == bot.BotId)} servers...");
            if (db != null)
                foreach (var server in db.Servers.Where(x => x.BotId == bot.BotId).ToList())
                    try
                    {
                        Console.WriteLine($"[{bot.Name}] Getting server: {server.DiscordServerId}");
                        var guild = await sender.GetGuildAsync(Convert.ToUInt64(server.DiscordServerId), true);
                        server.Name = guild.Name;
                        server.IconUrl = guild.IconUrl ?? "https://cdn.discordapp.com/embed/avatars/0.png";
                        server.MemberCount = (int) guild.ApproximateMemberCount;
                        server.OnlineMemberCount = (int) guild.ApproximatePresenceCount;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"[{bot.Name}] {e.Message}: " + server.DiscordServerId);
                    }

            //set bot user counts
            bot.OnlineMemberCount = bot.Servers.Sum(x => x.OnlineMemberCount);
            bot.MemberCount = bot.Servers.Sum(x => x.MemberCount);
            bot.ServerCount = c.Guilds.Count;
            // ready++;
        };
        // c.Resumed += async (_, _) => {
        // Console.WriteLine("We resumed, we shouldn't be running for this long! Exiting...");
        // Environment.Exit(0);
        // };
        Console.WriteLine(bot.Name + " starting...");
        await c.ConnectAsync();
        return c;
    }
}