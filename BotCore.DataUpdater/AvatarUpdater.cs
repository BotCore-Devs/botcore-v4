using BotCore.DataModel;
using DSharpPlus;

namespace BotCore.DataUpdater;

public class AvatarUpdater
{
    private static readonly Random rnd = new();

    public static async Task Run(List<DiscordClient> cl, Db db)
    {
        Console.WriteLine("Fetching avatars");
        var i = 0;

        db.GlobalUsers.AsEnumerable()
            .Where(x => x.AvatarUrl.StartsWith("https://cdn.discordapp.com/embed/") && rnd.Next(101) <= 5).ToList()
            .ForEach(x => x.AvatarUrl = "");
        await db.SaveChangesAsync();
        var uc = db.GlobalUsers.Count(x => x.AvatarUrl == "");
        foreach (var u in db.GlobalUsers.Where(x => x.AvatarUrl == "" || x.Username == "" || x.Discriminator == "")
                     .ToList())
        {
            Console.WriteLine($"[{++i}/{uc}] Updating avatar for " + u.DiscordUserId);
            try
            {
                var du = await cl[i % cl.Count].GetUserAsync(Convert.ToUInt64(u.DiscordUserId));
                u.AvatarUrl = du.GetAvatarUrl(ImageFormat.WebP, 512);
                u.Username = du.Username;
                u.Discriminator = du.Discriminator;
            }
            catch
            {
                u.AvatarUrl = "https://cdn.discordapp.com/embed/avatars/0.webp?size=512";
                u.Discriminator = "0000";
                u.Username = "Deleted user";
            }
        }

        await db.SaveChangesAsync();
    }
}