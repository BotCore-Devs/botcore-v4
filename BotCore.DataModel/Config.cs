namespace BotCore.DataModel;

public class Config
{
    private Db _db;
    public Config(Db db)
    {
        _db = db;
    }

    public int GetInt(string key, int defaultValue)
    {
        var value = _db.ConfigValues.SingleOrDefault(x => x.Key == key);
        if (value == null)
        {
            Console.WriteLine($"Config variable {key} not set!");
            _db.ConfigValues.Add(value = new(key, defaultValue + ""));
            _db.SaveChanges();
        }
        return int.Parse(value.Value);
    }
    
    public string GetString(string key, string defaultValue)
    {
        var value = _db.ConfigValues.SingleOrDefault(x => x.Key == key);
        if (value == null)
        {
            Console.WriteLine($"Config variable {key} not set!");
            _db.ConfigValues.Add(value = new(key, defaultValue));
            _db.SaveChanges();
        }
        return value.Value;
    }
}