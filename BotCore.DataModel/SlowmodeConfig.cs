﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BotCore.DataModel;

[Table("slowmode_config")]
public class SlowmodeConfig
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long SlowmodeId { get; set; }
    public ulong DiscordChannelId { get; set; }
    public Server Server { get; set; }
    public TimeSpan SlowmodeTime { get; set; } = TimeSpan.Zero;
    public bool MessageEnabled { get; set; } = true;
    public bool ModsBypass { get; set; } = true;
}