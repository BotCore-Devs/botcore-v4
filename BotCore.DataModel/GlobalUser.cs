﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BotCore.DataModel;

[Table("global_user")]
public class GlobalUser
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int GlobalUserId { get; set; }

    //properties
    public string DiscordUserId { get; set; } = "0";
    public string Username { get; set; } = "";
    public string Discriminator { get; set; } = "0000";
    public bool IsBot { get; set; } = false;
    public int Credits { get; set; } = 10;
    public bool DebugInfo { get; set; } = false;
    public bool IsPremium { get; set; } = false;

    [Column(TypeName = "timestamp without time zone")]
    public DateTime PremiumSince { get; set; } = new();

    public TimeSpan PremiumDuration { get; set; } = TimeSpan.FromDays(30);
    public string AvatarUrl { get; set; } = "";

    [Column(TypeName = "timestamp without time zone")]
    public DateTime LastCreditsRedeem { get; set; } = new();

    //relations

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public List<DServerUser> DServerUsers { get; set; } = new();
}