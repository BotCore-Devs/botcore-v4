﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BotCore.DataModel.Migrations
{
    public partial class addservericonurl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IconUrl",
                table: "server",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "MemberCount",
                table: "server",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OnlineMemberCount",
                table: "server",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IconUrl",
                table: "server");

            migrationBuilder.DropColumn(
                name: "MemberCount",
                table: "server");

            migrationBuilder.DropColumn(
                name: "OnlineMemberCount",
                table: "server");
        }
    }
}
