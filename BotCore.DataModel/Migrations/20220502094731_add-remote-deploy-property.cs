﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BotCore.DataModel.Migrations
{
    public partial class addremotedeployproperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "RemotelyDeployed",
                table: "bot",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RemotelyDeployed",
                table: "bot");
        }
    }
}
