﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace BotCore.DataModel.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "bot",
                columns: table => new
                {
                    BotId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Token = table.Column<string>(type: "text", nullable: true),
                    ServerCount = table.Column<int>(type: "integer", nullable: false),
                    MemberCount = table.Column<int>(type: "integer", nullable: false),
                    OnlineMemberCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bot", x => x.BotId);
                });

            migrationBuilder.CreateTable(
                name: "global_user",
                columns: table => new
                {
                    GlobalUserId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DiscordUserId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    Username = table.Column<string>(type: "text", nullable: false),
                    Discriminator = table.Column<string>(type: "text", nullable: false),
                    IsBot = table.Column<bool>(type: "boolean", nullable: false),
                    Credits = table.Column<int>(type: "integer", nullable: false),
                    DebugInfo = table.Column<bool>(type: "boolean", nullable: false),
                    IsPremium = table.Column<bool>(type: "boolean", nullable: false),
                    PremiumSince = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    PremiumDuration = table.Column<TimeSpan>(type: "interval", nullable: false),
                    AvatarUrl = table.Column<string>(type: "text", nullable: false),
                    LastCreditsRedeem = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_global_user", x => x.GlobalUserId);
                });

            migrationBuilder.CreateTable(
                name: "server",
                columns: table => new
                {
                    ServerId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DiscordServerId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    BotId = table.Column<int>(type: "integer", nullable: false),
                    AutoroleEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LevelsEnabled = table.Column<bool>(type: "boolean", nullable: true),
                    LockdownEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    MessageCount = table.Column<int>(type: "integer", nullable: false),
                    ModerationEnabled = table.Column<bool>(type: "boolean", nullable: true),
                    LevelupMessagesEnabled = table.Column<bool>(type: "boolean", nullable: true),
                    RaidDefenseEnabled = table.Column<bool>(type: "boolean", nullable: true),
                    ResetOnLeave = table.Column<bool>(type: "boolean", nullable: false),
                    DisabledCommandMessageEnabled = table.Column<bool>(type: "boolean", nullable: true),
                    MinLevelForLeaderboard = table.Column<int>(type: "integer", nullable: false),
                    UnknownCommandMessageEnabled = table.Column<bool>(type: "boolean", nullable: true),
                    ContentFilterEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    MentionsEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    MentionTreshold = table.Column<int>(type: "integer", nullable: false),
                    InvitesEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    InviteTreshold = table.Column<int>(type: "integer", nullable: false),
                    EmotesEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    EmoteTreshold = table.Column<int>(type: "integer", nullable: false),
                    BadLangEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    BadLangTreshold = table.Column<int>(type: "integer", nullable: false),
                    Prefix = table.Column<string>(type: "text", nullable: false),
                    DisabledCommands = table.Column<List<string>>(type: "text[]", nullable: false),
                    WordFilter = table.Column<List<string>>(type: "text[]", nullable: false),
                    BannedNameParts = table.Column<List<string>>(type: "text[]", nullable: false),
                    IgnoredChannels = table.Column<List<string>>(type: "text[]", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_server", x => x.ServerId);
                    table.ForeignKey(
                        name: "FK_server_bot_BotId",
                        column: x => x.BotId,
                        principalTable: "bot",
                        principalColumn: "BotId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "d_server_user",
                columns: table => new
                {
                    UserId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MessageCount = table.Column<int>(type: "integer", nullable: false),
                    Xp = table.Column<int>(type: "integer", nullable: false),
                    XpMultiplier = table.Column<double>(type: "double precision", nullable: false),
                    Muted = table.Column<bool>(type: "boolean", nullable: false),
                    UnmuteBy = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Left = table.Column<bool>(type: "boolean", nullable: false),
                    BlockedWordCount = table.Column<long>(type: "bigint", nullable: false),
                    EmoteCount = table.Column<double>(type: "double precision", nullable: false),
                    InviteCount = table.Column<double>(type: "double precision", nullable: false),
                    MentionCount = table.Column<double>(type: "double precision", nullable: false),
                    BadLanguageCount = table.Column<double>(type: "double precision", nullable: false),
                    Nicknames = table.Column<List<string>>(type: "text[]", nullable: true),
                    DServerServerId = table.Column<long>(type: "bigint", nullable: false),
                    GlobalUserId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_d_server_user", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_d_server_user_global_user_GlobalUserId",
                        column: x => x.GlobalUserId,
                        principalTable: "global_user",
                        principalColumn: "GlobalUserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_d_server_user_server_DServerServerId",
                        column: x => x.DServerServerId,
                        principalTable: "server",
                        principalColumn: "ServerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "quote",
                columns: table => new
                {
                    QuoteId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MessageText = table.Column<string>(type: "character varying(2000)", maxLength: 2000, nullable: false),
                    AuthorId = table.Column<long>(type: "bigint", nullable: false),
                    Rating = table.Column<int>(type: "integer", nullable: false),
                    ChannelId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    ServerId = table.Column<long>(type: "bigint", nullable: false),
                    MessageId = table.Column<decimal>(type: "numeric(20,0)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_quote", x => x.QuoteId);
                    table.ForeignKey(
                        name: "FK_quote_d_server_user_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "d_server_user",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_quote_server_ServerId",
                        column: x => x.ServerId,
                        principalTable: "server",
                        principalColumn: "ServerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "warning",
                columns: table => new
                {
                    WarningId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AuthorId = table.Column<long>(type: "bigint", nullable: true),
                    UserId = table.Column<long>(type: "bigint", nullable: true),
                    Message = table.Column<string>(type: "character varying(2000)", maxLength: 2000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_warning", x => x.WarningId);
                    table.ForeignKey(
                        name: "FK_warning_d_server_user_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "d_server_user",
                        principalColumn: "UserId");
                    table.ForeignKey(
                        name: "FK_warning_d_server_user_UserId",
                        column: x => x.UserId,
                        principalTable: "d_server_user",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_d_server_user_DServerServerId",
                table: "d_server_user",
                column: "DServerServerId");

            migrationBuilder.CreateIndex(
                name: "IX_d_server_user_GlobalUserId",
                table: "d_server_user",
                column: "GlobalUserId");

            migrationBuilder.CreateIndex(
                name: "IX_quote_AuthorId",
                table: "quote",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_quote_ServerId",
                table: "quote",
                column: "ServerId");

            migrationBuilder.CreateIndex(
                name: "IX_server_BotId",
                table: "server",
                column: "BotId");

            migrationBuilder.CreateIndex(
                name: "IX_warning_AuthorId",
                table: "warning",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_warning_UserId",
                table: "warning",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "quote");

            migrationBuilder.DropTable(
                name: "warning");

            migrationBuilder.DropTable(
                name: "d_server_user");

            migrationBuilder.DropTable(
                name: "global_user");

            migrationBuilder.DropTable(
                name: "server");

            migrationBuilder.DropTable(
                name: "bot");
        }
    }
}
