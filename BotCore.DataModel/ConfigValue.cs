using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace BotCore.DataModel;

public class ConfigValue
{
    public ConfigValue(string key, string value)
    {
        Key = key;
        Value = value;
    }
    [Key]
    [NotNull]
    public string Key { get; set; } = "";
    public string Value { get; set; } = "";
}