﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BotCore.DataModel;

[Table("slowmode_data")]
public class SlowmodeData
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long SlowmodeDataId { get; set; }
    public SlowmodeConfig SlowmodeConfig { get; set; }
    public Server Server { get; set; }
    public DServerUser User { get; set; }
    [Column(TypeName = "timestamp without time zone")]
    public DateTime BlockedSince { get; set; }
}