﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BotCore.DataModel;

[Table("quote")]
public class Quote
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int QuoteId { get; set; }

    //properties
    [StringLength(2000)] public string MessageText { get; set; } = "";

    public long AuthorId { get; set; }
    public int Rating { get; set; }
    public ulong ChannelId { get; set; }
    public long ServerId { get; set; }

    public ulong MessageId { get; set; }

    //relations

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public DServerUser Author { get; set; } = null!;

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public Server Server { get; set; } = null!;
}