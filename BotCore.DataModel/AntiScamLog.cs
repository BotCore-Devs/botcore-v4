using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BotCore.DataModel;

[Table("anti_scam_log")]
public class AntiScamLog
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }
    public Server Server { get; set; }
    public DServerUser User { get; set; }
    public string MessageText { get; set; }
    public DateTime Time { get; set; } = DateTime.Now;
}