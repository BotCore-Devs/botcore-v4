﻿using System.Diagnostics;
using System.Web;
using BotCore.DbExtras;
using BotCore.Util;
using DSharpPlus.VoiceNext;
using YoutubeExplode;
using YoutubeExplode.Common;

namespace BotCore.Commands.Commands.Groups;

internal class MusicCommands : CommandGroup
{
    public static Command SummonCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "summon",
            Category = "Music",
            Description = "Summons bot to your current voice chat",
            action = async (me, ce) =>
            {
                var vne = me.Bot.DiscordClient.GetVoiceNext();
                if (vne.GetConnection(me.Channel.Guild) != null)
                {
                    await me.Channel.SendMessageAsync("Already connected in this server!");
                }
                else
                {
                    if ((await me.Channel.Guild.GetMemberAsync(me.Message.Author.Id)).VoiceState.Channel == null)
                    {
                        await me.Channel.SendMessageAsync("You must be connected to a voice channel!");
                    }
                    else
                    {
                        await vne.ConnectAsync((await me.Channel.Guild.GetMemberAsync(me.Message.Author.Id)).VoiceState
                            .Channel);
                        await me.Channel.SendMessageAsync("Connected!");
                    }
                }

                return null;
            }
        };
    }

    public static Command LeaveCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "leave",
            Category = "Music",
            Description = "Leaves current voice chat",
            action = async (me, ce) =>
            {
                var vne = me.Bot.DiscordClient.GetVoiceNext();
                if (vne.GetConnection(me.Channel.Guild) == null)
                {
                    await me.Channel.SendMessageAsync("Not connected in this server!");
                }
                else
                {
                    vne.GetConnection(me.Channel.Guild).Disconnect();
                    await me.Channel.SendMessageAsync("Disconnected!");
                }

                return null;
            }
        };
    }

    /* TODO: Implement Proper Queuing System, procrastination ensues... */
    public static Command QueueCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "queue",
            Category = "Music",
            Description = "Show or add songs to queue",
            action = (me, ce) =>
            {
                if (ce.Args.Count == 0)
                {
                    //unused?
                    //string queue = "";
                    //int i = 0;
                    //await me.Channel.SendMessageAsync($"Queue:```{ String.Join($"\n    {i++}. ",ServerMusicTracking.GetPlayer(me.Channel.Guild.Id).queue.queue.ToArray())}```");
                }

                return null;
            }
        };
    }

    public static Command PlayCommandDefinition(BotImplementation bot)
    {
        var cli = new YoutubeClient();
        return new()
        {
            Name = "play",
            Category = "Music",
            Description = "Queues a song",
            action = async (me, ce) =>
            {
                //get player
                var mp = ServerMusicTracking.GetPlayer(me.Channel.Guild.Id);
                //tell user to get in a vc
                var member = await me.Channel.Guild.GetMemberAsync(me.Message.Author.Id);
                if (member.VoiceState == null)
                {
                    await me.Channel.SendMessageAsync("You must be connected to a voice channel!");
                }
                //if playlist, get all videos and add
                else if (ce.Args[0].Contains("youtube.com/playlist"))
                {
                    //var pl = await cli.Playlists.GetVideosAsync("PLFr3c472VstyuniFqFD6KBtzQhLuT_BKO");
                    Console.WriteLine(ce.Args[0].Split("?list=")[1]);
                    var pl = await cli.Playlists.GetVideosAsync(ce.Args[0].Split("?list=")[1]);
                    foreach (var v in pl) await mp.Queue.Add(v.Url, null);

                    await me.Channel.SendMessageAsync($"Queued {pl.Count} videos from playlist");
                }
                //add yt video
                else if (ce.Args[0].Contains("youtube.com/watch"))
                {
                    var mi = await mp.Queue.Add(ce.Args[0], ce);
                    var qa = HttpUtility.ParseQueryString(ce.Args[0]);
                    var video = qa.Get("v");
                    video ??= qa.Get(0);
                    var vid = await cli.Videos.GetAsync(video);
                    me.Channel.SendMessageAsync($"Queued youtube video {vid.Title} ({vid.Duration}) (ID: {video})!");
                    mi.Name = vid.Title;
                    mi.Icon = vid.Thumbnails.First(_ => true).Url;
                }
                //ignore
                else
                {
                    await me.Channel.SendMessageAsync("No idea if I can handle this link, sorry");
                }

                mp.Play(me);
                return null;
            }
        };
    }

    public static Command LPlayCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "lplay",
            Category = "Owner",
            Description = "Play local file in vc",
            action = async (me, ce) =>
            {
                var vne = me.Bot.DiscordClient.GetVoiceNext();
                if (vne.GetConnection(me.Channel.Guild) != null)
                {
                    await me.Channel.SendMessageAsync("Already connected in this server!");
                }
                else
                {
                    if ((await me.Channel.Guild.GetMemberAsync(me.Message.Author.Id)).VoiceState.Channel == null)
                    {
                        await me.Channel.SendMessageAsync("You must be connected to a voice channel!");
                    }
                    else
                    {
                        await vne.ConnectAsync((await me.Channel.Guild.GetMemberAsync(me.Message.Author.Id)).VoiceState
                            .Channel);
                        await me.Channel.SendMessageAsync("Connected!");
                    }
                }

                var file = me.Message.Content.Replace(me.Server.Prefix + "lplay ", "");
                if (vne.GetConnection(me.Channel.Guild) == null)
                {
                    await me.Channel.SendMessageAsync("Not connected in this server!");
                }
                else
                {
                    await me.Channel.SendMessageAsync("Playing!");
                    var vnc = vne.GetConnection(me.Channel.Guild);
                    await vnc.SendSpeakingAsync();
                    
                    var psi = new ProcessStartInfo
                    {
                        FileName = "ffmpeg",
                        Arguments = $@"-i ""{file}"" -ac 2 -f s16le -ar 48000 pipe:1",
                        RedirectStandardOutput = true,
                        UseShellExecute = false
                    };
                    var ffmpeg = Process.Start(psi);
                    var ffout = ffmpeg.StandardOutput.BaseStream;

                    var txStream = vnc.GetTransmitSink();
                    await ffout.CopyToAsync(txStream);
                    await txStream.FlushAsync();

                    await vnc.SendSpeakingAsync(false); // we're not speaking anymore
                }

                return null;
            }
        };
    }
}