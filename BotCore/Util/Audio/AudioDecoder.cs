using System.Diagnostics;

namespace BotCore.Util.Audio;

public class AudioDecoder
{
    public static string CacheDir
    {
        get
        {
            var cacheDir = "./";
            while (!File.Exists(cacheDir + "DiscordBots.sln"))
            {
                cacheDir += "../";
                if (cacheDir.Length >= 50) //folder doesnt exist in 24 parent dirs
                {
                    throw new ArgumentNullException("Couldnt find root dir!");
                }
            }

            cacheDir += "cache/audio/";
            return new DirectoryInfo(cacheDir).FullName;
        }
    }

    public static Stream GetPCM(string id)
    {
        Directory.CreateDirectory(CacheDir + "pcm");
        Directory.CreateDirectory(CacheDir + "dl");

        if (!File.Exists($"{CacheDir}pcm/{id}.wav"))
        {
            var psi = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $@"-i {CacheDir}dl/{id} -ar 48000 -f wav {CacheDir}pcm/{id}.wav",
                RedirectStandardOutput = false,
                UseShellExecute = false,
                RedirectStandardInput = false
            };
            var ffmpeg = Process.Start(psi);
            ffmpeg.WaitForExit();
        }

        return File.OpenRead(CacheDir + "pcm/" + id + ".wav");
    }
}