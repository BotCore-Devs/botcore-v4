using BotCore.Classes;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;
using DSharpPlus.Entities;

namespace BotCore.Util;

public static class MessageEnvironmentBuilder
{
    public static MessageEnvironment Build(BotImplementation bot, DiscordMessage msg)
    {
        var environment = new MessageEnvironment
        {
            Bot = bot,
            Message = msg,
            Channel = msg.Channel,
            Db = bot.Db
        };
        //TODO: fix defaults
        //get user
        environment.User = environment.Db.GetGlobalUser(environment.Message.Author.Id);
        //get server
        environment.Server = environment.Db.GetServer(environment.Bot.Bot, environment.Channel.GuildId.Value);
        //get server user
        environment.DSUser = environment.Db.GetServerUser(environment.Server, environment.User);
        //fetch data for first mentioned user, if any, otherwise just use author
        environment.MentionedDSUser = environment.DSUser;
        if (environment.Message.MentionedUsers.Count > 0)
        {
            //get user
            environment.MentionedUser = environment.Db.GetGlobalUser(environment.Message.MentionedUsers[0].Id);
            //get server user
            environment.MentionedDSUser = environment.Db.GetServerUser(environment.Server, environment.MentionedUser);
        }

        return environment;
    }
}