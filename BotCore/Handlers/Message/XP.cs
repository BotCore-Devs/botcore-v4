﻿using System.Diagnostics;
using BotCore.Classes;
using DSharpPlus.Entities;
using DSharpPlus.Exceptions;

namespace BotCore.Handlers.Message;

public class Xp
{
    private static readonly Random rnd = new();

    public static async void CheckMessage(MessageEnvironment environment)
    {
        //XP system
        var newxp = 0;
        var words = environment.Message.Content.Split(' ');
        if (!environment.Message.Author.IsBot && (environment.Server.LevelsEnabled ?? false))
        {
            newxp = (int) (rnd.Next(25, 75) * (environment.Message.Content.Length > words.Length * 3 ? 1 : 0) *
                           (words.Length < 75 ? words.Length / 10d : (100 - words.Length) / 50d));
            environment.DSUser.Xp += (int) (newxp * environment.DSUser.XpMultiplier);
            if (environment.DSUser.Xp >= environment.DSUser.XpGoal)
            {
                while (environment.DSUser.Xp >= environment.DSUser.XpGoal)
                {
                    environment.DSUser.Xp -= environment.DSUser.XpGoal;
                    environment.DSUser.XpLevel += 1;
                }

                SendLevelupMessage(environment);
            }
        }
    }

    private static async void SendLevelupMessage(MessageEnvironment environment)
    {
        if (environment.Server.LevelupMessagesEnabled ?? false)
            try
            {
                var message = await environment.Message.Channel.SendMessageAsync(
                    $"Congratulations! {environment.DSUser.GlobalUser.Username} just achieved level {environment.DSUser.XpLevel}! :partying_face:");
                new Thread(() =>
                {
                    Thread.Sleep(10000);
                    message.DeleteAsync();
                }).Start();
            }
            catch (UnauthorizedException)
            {
                try
                {
                    await environment.Message.CreateReactionAsync(DiscordEmoji.FromName(
                        environment.Bot.DiscordClient,
                        ":partying_face:"));
                }
                catch
                {
                    Debug.WriteLine("XP announcement broke");
                }
            }
    }
}