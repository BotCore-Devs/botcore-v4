﻿using BotCore.Classes;
using BotCore.DataModel;
using BotCore.Util.Extensions.DSharpPlus;
using DSharpPlus;

namespace BotCore.Handlers.Message;

public class CustomSlowmodeHandler
{
    public static async void CheckMessage(MessageEnvironment me)
    {
        if (me.Message.Author.IsBot) return;
        var (config, userdata) = GetData(me);
        if (config is null) return;
        if (userdata is null)
        {
            me.Db.SlowmodeDatas.Add(new()
            {
                Server = me.Server,
                User = me.DSUser,
                BlockedSince = DateTime.Now,
                SlowmodeConfig = config
            });
            return;
        }

        if (DateTime.Now.Subtract(userdata.BlockedSince) < config.SlowmodeTime)
        {
            if (config.ModsBypass && me.Message.Channel.PermissionsFor(await me.Message.GetAuthorAsMemberAsync())
                    .HasFlag(Permissions.ManageGuild)) return;
            try
            {
                await me.Message.DeleteAsync();
                SendMessage(me, config, userdata);
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not delete message (slowmode):");
                Console.WriteLine(e);
            }
            
            return;
        }
        me.Db.SlowmodeDatas.Remove(userdata);
    }

    private static (SlowmodeConfig? config, SlowmodeData? data) GetData(MessageEnvironment me)
    {
        var config =
            me.Db.SlowmodeConfigs.SingleOrDefault(x => x.DiscordChannelId == me.Channel.Id && x.Server == me.Server);
        var userdata = me.Db.SlowmodeDatas.SingleOrDefault(x =>
            x.Server == me.Server && x.SlowmodeConfig == config && x.User == me.DSUser);
        return (config, userdata);
    }
    private static async void SendMessage(MessageEnvironment me, SlowmodeConfig config, SlowmodeData userdata)
    {
        if (config.MessageEnabled)
        {
            //user posted within delay, show how long to wait
            var waitTime = config.SlowmodeTime - DateTime.Now.Subtract(userdata.BlockedSince);
            string waitTimeStr = "You need to wait ";
            if (waitTime.TotalDays >= 1) waitTimeStr += $"{Math.Floor(waitTime.TotalDays)} days, ";
            if (waitTime.Hours > 0) waitTimeStr += $"{waitTime.Hours} hours, ";
            if (waitTime.Minutes > 0) waitTimeStr += $"{waitTime.Minutes} minutes, and ";
            waitTimeStr += $"{waitTime.Seconds} seconds.";
            var msg = await me.Channel.SendMessageAsync(waitTimeStr);
            new Thread(() =>
            {
                Thread.Sleep(10000);
                msg.DeleteAsync();
            }).Start();
        }
    }
}