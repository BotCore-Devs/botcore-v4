using BotCore.Commands;
using BotCore.Commands.Commands;
using BotCore.DbExtras;

namespace Bots.FosscordBot.Commands;

public class Github : CommandGroup
{
    //g, return github pull request or issue for repo
    public static Command PRIssueCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "g",
            Category = "Github",
            Description = "Return github pull request/issue link for repo",
            action = async (me, ce) =>
            {
                await me.Channel.SendMessageAsync(
                    $"https://github.com/spacebarchat/{ce.Args[0]}/issues/{ce.Args[1]}");
                return null;
            }
        };
    }

    //c, return commit for repo
    public static Command CommitCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "c",
            Category = "Github",
            Description = "Return github commit link for repo",
            action = async (me, ce) =>
            {
                await me.Channel.SendMessageAsync(
                    $"https://github.com/spacebarchat/{ce.Args[0]}/commit/{ce.Args[1]}");
                return null;
            }
        };
    }
}