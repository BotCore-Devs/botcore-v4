namespace Bots.SugarcaneBot.DataStructures.Timings.timings.aikar.co.Recommendations;

public class AikarRecommendations
{
    public static List<TimingsRecommendations> GetRecommendations(AikarTimingsRoot timings)
    {
        List<TimingsRecommendations> recommendationsList = new();
        recommendationsList.AddRange(AikarPluginRecommendations.GetRecommendations(timings));
        recommendationsList.AddRange(AikarSystemRecommendations.GetRecommendations(timings));
        recommendationsList.AddRange(AikarConfigRecommendations.GetRecommendations(timings));
        return recommendationsList;
    }
}